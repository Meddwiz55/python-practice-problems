# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.


def has_quorum(attendees_list, members_list):
    half = members_list / 2
    if attendees_list >= half:
        print("half or more members are attending")
        return True
    else:
        print("less than half of the members showed up today")
        return False
