# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"


def gear_for_day(is_workday, is_sunny):
    gear = []
    if not is_sunny and is_workday:
        print("not sunny and workday, get umbrella")
        gear.append("umbrella")
    if is_workday:
        print("its a workday")
        gear.append("Laptop")
    if not is_workday:
        print("Weekend, time to surf!!!")
        gear.append("surfboard")

    return print(", ".join(gear))


# gear_for_day(True, False)
# gear_for_day(False, True)
# gear_for_day(False, False)
# gear_for_day(True, True)
